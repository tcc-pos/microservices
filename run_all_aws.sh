nohup java -jar ./eureka-server/target/eureka-server-1.0.0.jar >> microservices.log &;
nohup java -jar ./user-ms/target/user-ms-0.0.1.jar >> microservices.log &;
nohup java -jar ./saemIntegration/target/saemIntegration-0.0.1.jar >> microservices.log &;
nohup java -jar ./impostosIntegration/target/impostosIntegration-0.0.1.jar >> microservices.log &;
nohup java -jar ./consultasMedicasIntegration/target/consultasMedicasIntegration-0.0.1.jar >> microservices.log &;
nohup java -jar ./api-gateway/target/api-gateway-0.0.1.jar >> microservices.log &;
