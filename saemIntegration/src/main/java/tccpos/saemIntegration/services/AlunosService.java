package tccpos.saemIntegration.services;

import tccpos.saemIntegration.domain.Aluno;
import tccpos.saemIntegration.exceptions.InvalidDataException;

public interface AlunosService {

	public Iterable<Aluno> find(final Long cpf) throws InvalidDataException;

}
