package tccpos.saemIntegration.services;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import tccpos.saemIntegration.domain.Aluno;
import tccpos.saemIntegration.domain.AlunoSaem;
import tccpos.saemIntegration.domain.GradeSaem;
import tccpos.saemIntegration.domain.Materia;
import tccpos.saemIntegration.domain.UnidadeEnsino;
import tccpos.saemIntegration.domain.UnidadeEnsinoSaem;
import tccpos.saemIntegration.exceptions.InvalidDataException;
import tccpos.saemIntegration.integrations.SaemProxy;

@Service
@Slf4j
public class AlunosServiceImp implements AlunosService {

	@Autowired
	private SaemProxy saemProxy;

	public Iterable<Aluno> find(final Long cpf) throws InvalidDataException {
		List<AlunoSaem> lAlunosTmp = saemProxy.findAlunosByCpf(cpf);
		lAlunosTmp.addAll(saemProxy.findAlunosByCpfResponsavel(cpf));
		List<AlunoSaem> lAlunos = lAlunosTmp.stream().filter(distinctByKey(a -> a.getId()))
				.collect(Collectors.toList());

		log.info("{}", lAlunos);

		return lAlunos.parallelStream().map(a -> convertAlunoSaemToAluno(a)).collect(Collectors.toList());
	}

	private Materia convertGradeSaemToMateria(GradeSaem gradeSaem) {
		Materia materia = new Materia();
		materia.setCodigo(gradeSaem.getMateriaId());
		try {
			materia.setNome(saemProxy.findMateriaByID(gradeSaem.getMateriaId()).getNome());
		} catch (InvalidDataException e) {
			log.error("{}", e);
		}
		for (int i = 0; i < gradeSaem.getAvaliacoes().length; i++) {
			switch (i) {
			case 1:
				materia.setNota1(i);
				break;
			case 2:
				materia.setNota2(i);
				break;
			case 3:
				materia.setNota3(i);
				break;
			default:
				materia.setNota4(i);
				break;
			}
		}
		return materia;
	}

	private Aluno convertAlunoSaemToAluno(AlunoSaem alunoSaem) {
		Aluno aluno = new Aluno();
		alunoSaem.getLComponenteCurricularSaem().parallelStream().forEach(c -> {
			List<Materia> lMateria = c.getLGradeSaem().parallelStream().map(g -> convertGradeSaemToMateria(g))
					.collect(Collectors.toList());
			aluno.setAno(c.getAno());
			aluno.setCpfAluno(alunoSaem.getCpfAluno());
			aluno.setCpfResponsavel(alunoSaem.getCpfResponsavel());
			aluno.setFaltas(c.getFaltas());
			aluno.setId(alunoSaem.getId());
			aluno.setMatricula(alunoSaem.getMatricula());
			aluno.setNascimento(alunoSaem.getNascimento());
			aluno.setNome(alunoSaem.getNome());
			aluno.setComponenteCurricular(lMateria);
			try {
				aluno.setSerieAtual(saemProxy.findClasseByID(c.getClasseId()).getNome());
				UnidadeEnsino unidadeEnsino = new UnidadeEnsino();
				UnidadeEnsinoSaem unidadeEnsinoSaem = saemProxy.findUnidadeEnsinoByID(c.getUnidadeEnsinoId());
				unidadeEnsino.setCodUE(unidadeEnsinoSaem.getId());
				unidadeEnsino.setUnidadeEnsino(unidadeEnsinoSaem.getNome());
				aluno.setUnidadeEnsino(unidadeEnsino);
			} catch (InvalidDataException e) {
				log.error("{}", e);
			}

		});

		return aluno;
	}

	private <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

}
