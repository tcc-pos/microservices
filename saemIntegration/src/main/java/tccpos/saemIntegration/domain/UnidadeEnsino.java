package tccpos.saemIntegration.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UnidadeEnsino implements Serializable {

	private static final long serialVersionUID = 807562934471636624L;
	private Long codUE;
	private String unidadeEnsino;
	private String enderecoUE;
	private String cepUE;

}
