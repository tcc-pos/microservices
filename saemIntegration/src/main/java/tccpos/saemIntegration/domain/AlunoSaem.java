package tccpos.saemIntegration.domain;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlunoSaem {

	private Long id;
	@JsonProperty("cpf_aluno")
	private Long cpfAluno;
	private Long matricula;
	private LocalDate nascimento;
	private String nome;
	@JsonProperty("cpf_responsavel")
	private Long cpfResponsavel;
	@JsonProperty("componete_curricular")
	private List<ComponenteCurricularSaem> lComponenteCurricularSaem;

}