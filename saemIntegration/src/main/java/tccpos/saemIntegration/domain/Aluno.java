package tccpos.saemIntegration.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Aluno implements Serializable {

	private static final long serialVersionUID = -2799592047765246241L;
	private Long id;
	private Long matricula;
	private String nome;
	private LocalDate nascimento;
	private Long cpfAluno;
	private String serieAtual;
	private Long ano;
	private Integer ch;
	private Integer totalDias;
	private Long faltas;
	private Long cpfResponsavel;
	private UnidadeEnsino unidadeEnsino;
	private List<Materia> componenteCurricular;

}
