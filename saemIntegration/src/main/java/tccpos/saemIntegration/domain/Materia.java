package tccpos.saemIntegration.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Materia implements Serializable {

	private static final long serialVersionUID = 629624789352142321L;
	private Long codigo;
	private String nome;
	private Integer nota1;
	private Integer nota2;
	private Integer nota3;
	private Integer nota4;

}
