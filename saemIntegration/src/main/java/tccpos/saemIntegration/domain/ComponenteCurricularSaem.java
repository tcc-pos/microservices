package tccpos.saemIntegration.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComponenteCurricularSaem {

	private Long id;
	private Long ano;
	private Long faltas;
	@JsonProperty("unidade_ensino_id")
	private Long unidadeEnsinoId;
	@JsonProperty("classe_id")
	private Long classeId;
	@JsonProperty("grade")
	private List<GradeSaem> lGradeSaem;

}
