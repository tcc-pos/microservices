package tccpos.saemIntegration.exceptions;

import lombok.Getter;

public class NotFoundException extends Exception {

	private static final long serialVersionUID = 3857698137429235523L;

	@Getter
	private final String[] errors;

	public NotFoundException(final String[] erros) {
		super();
		this.errors = erros;
	}

	public NotFoundException() {
		super();
		this.errors = new String[] { "Not Found" };
	}

}
