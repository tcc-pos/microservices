package tccpos.saemIntegration.integrations;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

@Component
public class TokenFilter implements Filter {

	public static String TOKEN;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		TOKEN = ((HttpServletRequest) request).getHeader("authorization");

		chain.doFilter(request, response);

	}

}
