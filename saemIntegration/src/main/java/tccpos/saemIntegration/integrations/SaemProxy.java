package tccpos.saemIntegration.integrations;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import tccpos.saemIntegration.domain.UnidadeEnsinoSaem;
import tccpos.saemIntegration.exceptions.InvalidDataException;

import tccpos.saemIntegration.domain.AlunoSaem;
import tccpos.saemIntegration.domain.ClasseSaem;
import tccpos.saemIntegration.domain.MateriaSaem;

import java.util.List;

@FeignClient(name = "saemProxy", url = "${saem-url}")
public interface SaemProxy {

	@GetMapping(value = "/alunos")
	public List<AlunoSaem> findAlunosByCpf(@RequestParam("cpf_aluno") final Long cpf) throws InvalidDataException;

	@GetMapping(value = "/alunos")
	public List<AlunoSaem> findAlunosByCpfResponsavel(@RequestParam("cpf_responsavel") final Long cpf)
			throws InvalidDataException;

	@GetMapping(value = "unidade_ensino/{id}")
	public UnidadeEnsinoSaem findUnidadeEnsinoByID(@PathVariable(value = "id") final Long id)
			throws InvalidDataException;

	@GetMapping(value = "clases/{id}")
	public ClasseSaem findClasseByID(@PathVariable(value = "id") final Long id) throws InvalidDataException;

	@GetMapping(value = "materias/{id}")
	public MateriaSaem findMateriaByID(@PathVariable(value = "id") final Long id) throws InvalidDataException;

}
