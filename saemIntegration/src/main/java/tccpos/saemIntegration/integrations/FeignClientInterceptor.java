package tccpos.saemIntegration.integrations;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import feign.RequestInterceptor;
import feign.RequestTemplate;

@Component
public class FeignClientInterceptor implements RequestInterceptor {

	private static final String AUTHORIZATION_HEADER = "Authorization";

	@Override
	public void apply(RequestTemplate requestTemplate) {
		if (!StringUtils.isEmpty(TokenFilter.TOKEN)) {
			requestTemplate.header(AUTHORIZATION_HEADER, TokenFilter.TOKEN);
		}
	}
}