package tccpos.saemIntegration.controller;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import tccpos.saemIntegration.domain.Aluno;
import tccpos.saemIntegration.exceptions.InvalidDataException;
import tccpos.saemIntegration.services.AlunosService;

@RestController
@RequestMapping({ "/alunos" })
@Tag(name = "alunos")
public class AlunosController {

	@Autowired
	private AlunosService alunosServices;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponse(responseCode = "200", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Aluno.class))))
	public Iterable<Aluno> findByCpf(@QueryParam(value = "cpf") final Long cpf) throws InvalidDataException {
		return alunosServices.find(cpf);
	}

}
