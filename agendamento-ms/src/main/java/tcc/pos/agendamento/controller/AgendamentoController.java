package tcc.pos.agendamento.controller;

import java.time.LocalDate;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import tcc.pos.agendamento.domain.Agendamento;
import tcc.pos.agendamento.exceptions.InvalidDataException;
import tcc.pos.agendamento.exceptions.NotFoundException;
import tcc.pos.agendamento.services.AgendamentoService;

@RestController
@RequestMapping
@Tag(name = "agendamentos")
public class AgendamentoController {

	@Autowired
	private AgendamentoService agendamentoService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Iterable<Agendamento> find(@QueryParam(value = "data") final LocalDate data) {
		return agendamentoService.find(data);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Agendamento findById(@PathVariable final Long id) throws NotFoundException {
		return agendamentoService.findById(id);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.CREATED)
	public Agendamento create(@RequestBody final Agendamento agendamento) throws InvalidDataException {
		return agendamentoService.create(agendamento);
	}

	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Agendamento update(@PathVariable("id") final Long id, @RequestBody final Agendamento agendamento)
			throws NotFoundException, InvalidDataException {
		return agendamentoService.update(id, agendamento);
	}

	@DeleteMapping(value = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") final Long id) throws NotFoundException {
		agendamentoService.delete(id);
	}

}
