package tcc.pos.agendamento.integrations;

import org.springframework.stereotype.Component;

import feign.RequestInterceptor;
import feign.RequestTemplate;

@Component
public class FeignClientInterceptor implements RequestInterceptor {

	private static final String AUTHORIZATION_HEADER = "Authorization";

	@Override
	public void apply(RequestTemplate requestTemplate) {
		requestTemplate.header(AUTHORIZATION_HEADER, TokenFilter.TOKEN);
	}
}