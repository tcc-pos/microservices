package tcc.pos.agendamento.integrations;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import tcc.pos.agendamento.domain.Persona;
import tcc.pos.agendamento.exceptions.InvalidDataException;

@FeignClient(name = "GuicheProxy", url = "${guiches-url}")
public interface GuicheProxy {

	@GetMapping(value = "/{id}")
	public Persona findByID(@PathVariable(value = "id") final Long id) throws InvalidDataException;

}
