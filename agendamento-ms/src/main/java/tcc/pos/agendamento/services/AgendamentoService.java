package tcc.pos.agendamento.services;

import java.time.LocalDate;
import java.util.stream.StreamSupport;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tcc.pos.agendamento.domain.Agendamento;
import tcc.pos.agendamento.exceptions.InvalidDataException;
import tcc.pos.agendamento.exceptions.NotFoundException;
import tcc.pos.agendamento.integrations.GuicheProxy;
import tcc.pos.agendamento.integrations.PersonaProxy;
import tcc.pos.agendamento.repository.AgendamentoRepository;

@Service
public class AgendamentoService {

	@Autowired
	private AgendamentoRepository agendamentoRepository;

	@Autowired
	private PersonaProxy personaProxy;

	@Autowired
	private GuicheProxy guicheProxy;

	public Iterable<Agendamento> find(final LocalDate data) {
		if (data == null) {
			return agendamentoRepository.findAll();
		} else {
			return agendamentoRepository.findByData(data);
		}
	}

	public Agendamento findById(final Long id) throws NotFoundException {
		return agendamentoRepository.findById(id).orElseThrow(() -> new NotFoundException());
	}

	public Agendamento create(final Agendamento agendamento) throws InvalidDataException {
		personaProxy.findByID(agendamento.getIdPersona());
		guicheProxy.findByID(agendamento.getIdGuiche());

		Long count = StreamSupport.stream(agendamentoRepository
				.findByIdGuicheAndData(agendamento.getIdGuiche(), agendamento.getData()).spliterator(), false).count();

		if (count > 0L) {
			throw new InvalidDataException(new String[] { "Horario e guiche ocupados" });
		}

		return agendamentoRepository.save(agendamento);
	}

	public Agendamento update(final Long id, final Agendamento agendamento)
			throws NotFoundException, InvalidDataException {
		Agendamento agendamentoEntity = agendamentoRepository.findById(id).orElseThrow(() -> new NotFoundException());
		BeanUtils.copyProperties(agendamento, agendamentoEntity);

		personaProxy.findByID(agendamento.getIdPersona());
		guicheProxy.findByID(agendamento.getIdGuiche());

		Long count = StreamSupport.stream(agendamentoRepository
				.findByIdGuicheAndData(agendamento.getIdGuiche(), agendamento.getData()).spliterator(), false).count();

		if (count > 0L) {
			throw new InvalidDataException(new String[] { "Horario e guiche ocupados" });
		}

		return agendamentoRepository.save(agendamentoEntity);
	}

	public void delete(final Long id) throws NotFoundException {
		agendamentoRepository.delete(agendamentoRepository.findById(id).orElseThrow(() -> new NotFoundException()));
	}

}
