package tcc.pos.agendamento.repository;

import java.time.LocalDate;

import org.springframework.data.repository.CrudRepository;

import tcc.pos.agendamento.domain.Agendamento;

public interface AgendamentoRepository extends CrudRepository<Agendamento, Long> {

	public Iterable<Agendamento> findByIdGuicheAndData(final Long idGuiche, final LocalDate data);
	public Iterable<Agendamento> findByData(final LocalDate data);

}
