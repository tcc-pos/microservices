package tcc.pos.agendamento.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_EMPTY)
public class Agendamento implements Serializable {

	private static final long serialVersionUID = -2799592047765246241L;
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "id_persona")
	private Long idPersona;
	@Column(name = "id_guiche")
	private Long idGuiche;
	private LocalDate data;
	

}
