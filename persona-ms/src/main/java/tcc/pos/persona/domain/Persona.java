package tcc.pos.persona.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_EMPTY)
public class Persona implements Serializable {

	private static final long serialVersionUID = -2799592047765246241L;
	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	private Long documento;
	//@NotNull
	private String nome;
	private Long[] propriedades;
	@Column(name = "sub_id", unique = true)
	@JsonIgnore
	private String subId;

}
