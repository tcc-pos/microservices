package tcc.pos.persona.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import tcc.pos.persona.exceptions.AccessDeniedException;

public class UserValidation {

	public static void validateUserAccess(final Long id) throws AccessDeniedException {
		if (Long.valueOf(getSub2()) != id && !isAdmin()) {
			throw new AccessDeniedException();
		}
	}

	public static Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}
	
	public static String getSub2() {
		return getAuthentication().getName();
	}

	public static Boolean isAdmin() {
		return isAdmin(getAuthentication());
	}

	@SuppressWarnings("unlikely-arg-type")
	public static Boolean isAdmin(final Authentication authentication) {
		return authentication.getAuthorities().contains("ADMIN");
	}

}
