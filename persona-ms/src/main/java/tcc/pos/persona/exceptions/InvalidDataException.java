package tcc.pos.persona.exceptions;

import lombok.Getter;

public class InvalidDataException extends Exception {

	private static final long serialVersionUID = 4896802999118701962L;

	@Getter
	private final String[] errors;

	public InvalidDataException(final String[] erros) {
		super();
		this.errors = erros;
	}

	public InvalidDataException() {
		super();
		this.errors = new String[] { "Invalid data" };
	}

}
