package tcc.pos.persona.services;

import tcc.pos.persona.domain.Persona;
import tcc.pos.persona.exceptions.AccessDeniedException;
import tcc.pos.persona.exceptions.InvalidDataException;
import tcc.pos.persona.exceptions.NotFoundException;

public interface PersonaService {

	public Iterable<Persona> find(final Long documento, final String subId)
			throws InvalidDataException, AccessDeniedException;

	public Persona findById(final Long id) throws NotFoundException;

	public Persona create(final Persona persona) throws InvalidDataException;

	public Persona update(final Long id, final Persona persona) throws NotFoundException, InvalidDataException;

	public void delete(final Long id) throws NotFoundException, InvalidDataException;

}
