package tcc.pos.persona.services;

import java.util.stream.StreamSupport;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import tcc.pos.persona.domain.Persona;
import tcc.pos.persona.exceptions.AccessDeniedException;
import tcc.pos.persona.exceptions.InvalidDataException;
import tcc.pos.persona.exceptions.NotFoundException;
import tcc.pos.persona.integrations.PropriedadeProxy;
import tcc.pos.persona.repository.PersonaRepository;
import tcc.pos.persona.utils.UserValidation;

@Service
public class PersonaServiceImp implements PersonaService {

	@Autowired
	private PersonaRepository personaRepository;

	@Autowired
	private PropriedadeProxy propriedadeProxy;

	public Iterable<Persona> find(final Long documento, final String subId)
			throws InvalidDataException, AccessDeniedException {
		if (!UserValidation.isAdmin() && !StringUtils.isEmpty(subId)) {
			throw new AccessDeniedException();
		}

		if (StringUtils.isEmpty(documento)) {
			return personaRepository.findAll();
		} else if (StringUtils.isEmpty(subId)) {
			return personaRepository.findBySubId(subId);
		} else {
			return personaRepository.findByDocumento(documento);
		}
	}

	public Persona findById(final Long id) throws NotFoundException {
		return personaRepository.findById(id).orElseThrow(() -> new NotFoundException());
	}

	public Persona create(final Persona persona) throws InvalidDataException {
		Long count = StreamSupport.stream(personaRepository.findBySubId(UserValidation.getSub2()).spliterator(), false)
				.count();

		if (count != null && count > 0) {
			throw new InvalidDataException(new String[] { "User already exists" });
		}

		persona.setSubId(UserValidation.getSub2());
		if (persona.getPropriedades() != null) {
			for (Long propriedadeId : persona.getPropriedades()) {
				propriedadeProxy.findByID(propriedadeId);
			}
		}
		return personaRepository.save(persona);
	}

	public Persona update(final Long id, final Persona persona) throws NotFoundException, InvalidDataException {
		Persona personaEntity = personaRepository.findById(id).orElseThrow(() -> new NotFoundException());

		if (!persona.getSubId().equals(personaEntity.getSubId())) {
			throw new InvalidDataException(new String[] { "cant change subId" });
		}

		BeanUtils.copyProperties(persona, personaEntity);

		if (persona.getPropriedades() != null) {
			for (Long propriedadeId : persona.getPropriedades()) {
				propriedadeProxy.findByID(propriedadeId);
			}
		}

		return personaRepository.save(personaEntity);
	}

	public void delete(final Long id) throws NotFoundException, InvalidDataException {
		Persona persona = personaRepository.findById(id).orElseThrow(() -> new NotFoundException());

		for (Long propriedadeId : persona.getPropriedades()) {
			propriedadeProxy.delete(propriedadeId);
		}

		personaRepository.delete(persona);
	}

}
