package tcc.pos.persona.controller;

import javax.validation.Valid;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import tcc.pos.persona.domain.Persona;
import tcc.pos.persona.exceptions.AccessDeniedException;
import tcc.pos.persona.exceptions.InvalidDataException;
import tcc.pos.persona.exceptions.NotFoundException;
import tcc.pos.persona.services.PersonaService;

@RestController
@RequestMapping
@Tag(name = "personas")
@Validated
public class PersonaController {

	@Autowired
	private PersonaService personaService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Iterable<Persona> findByCpf(@QueryParam(value = "documento") final Long documento,
			@QueryParam(value = "sub_id") final String subId) throws InvalidDataException, AccessDeniedException {
		return personaService.find(documento, subId);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Persona findById(@PathVariable final Long id) throws NotFoundException {
		return personaService.findById(id);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.CREATED)
	public Persona create(@Valid @RequestBody final Persona persona) throws InvalidDataException {
		return personaService.create(persona);
	}

	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Persona update(@PathVariable("id") final Long id, @Valid @RequestBody final Persona persona)
			throws NotFoundException, InvalidDataException {
		return personaService.update(id, persona);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(value = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") final Long id) throws NotFoundException, InvalidDataException {
		personaService.delete(id);
	}

}
