package tcc.pos.persona.repository;

import org.springframework.data.repository.CrudRepository;

import tcc.pos.persona.domain.Persona;

public interface PersonaRepository extends CrudRepository<Persona, Long> {

	public Iterable<Persona> findByDocumento(final Long documento);
	public Iterable<Persona> findBySubId(final String subId);

}
