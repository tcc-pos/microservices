package tcc.pos.persona.integrations;

import org.springframework.stereotype.Component;


import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import tcc.pos.persona.exceptions.InvalidDataException;

@Component
@Slf4j
public class FeignErrorDecoder implements ErrorDecoder {

	@Override
	public Exception decode(String methodKey, Response response) {

		switch (response.status()) {
		case 400:
		case 404: {
			log.error(response.toString());
			return new InvalidDataException();
		}
		default:
			log.error(response.toString());
			return new Exception(response.reason());
		}
	}

}