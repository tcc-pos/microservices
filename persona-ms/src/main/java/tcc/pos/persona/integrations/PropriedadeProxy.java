package tcc.pos.persona.integrations;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import tcc.pos.persona.domain.Propriedade;
import tcc.pos.persona.exceptions.InvalidDataException;

@FeignClient(name = "PropriedadeProxy", url = "${propriedades-url}")
public interface PropriedadeProxy {

	@GetMapping(value = "/{id}")
	public Propriedade findByID(@PathVariable(value = "id") final Long id) throws InvalidDataException;

	@DeleteMapping(value = "/{id}")
	public void delete(@PathVariable(value = "id") final Long id) throws InvalidDataException;

}
