mvn clean package -f ./api-gateway/ -DskipTests=true;
mvn clean package -f ./user-ms/ -DskipTests=true;
mvn clean package -f ./saem/ -DskipTests=true;
mvn clean package -f ./IptuMS/ -DskipTests=true;
mvn clean package -f ./guiche-ms/ -DskipTests=true;
mvn clean package -f ./ConsultasMedicasMS/ -DskipTests=true;
mvn clean package -f ./agendamento-ms/ -DskipTests=true;
docker build --tag tcc-pos/frontend:0.0.1 ../frontend/.
