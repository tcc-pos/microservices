mvn clean package -f ./api-gateway/ -DskipTests=true;
mvn clean package -f ./user-ms/ -DskipTests=true;
mvn clean package -f ./saemIntegration/ -DskipTests=true;
mvn clean package -f ./impostosIntegration/ -DskipTests=true;
mvn clean package -f ./consultasMedicasIntegration/ -DskipTests=true;
mvn clean package -f ./eureka-server/ -DskipTests=true;
