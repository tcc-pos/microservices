package tccpos.impostosIntegration.controller;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import tccpos.impostosIntegration.domain.Proprietario;
import tccpos.impostosIntegration.exceptions.InvalidDataException;
import tccpos.impostosIntegration.services.ProprietariosService;

@RestController
@RequestMapping({ "/proprietarios" })
@Tag(name = "proprietarios")
public class ProprietariosController {

	@Autowired
	private ProprietariosService proprietariosServices;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponse(responseCode = "200", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Proprietario.class))))
	public Iterable<Proprietario> findByCpf(@QueryParam(value = "documento") final String documento)
			throws InvalidDataException {
		return proprietariosServices.find(documento);
	}

}
