package tccpos.impostosIntegration.services;

import tccpos.impostosIntegration.domain.Proprietario;
import tccpos.impostosIntegration.exceptions.InvalidDataException;

public interface ProprietariosService {

	public Iterable<Proprietario> find(final String documento) throws InvalidDataException;

}
