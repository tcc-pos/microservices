package tccpos.impostosIntegration.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import tccpos.impostosIntegration.domain.Imovel;
import tccpos.impostosIntegration.domain.ImovelIntegration;
import tccpos.impostosIntegration.domain.Imposto;
import tccpos.impostosIntegration.domain.Proprietario;
import tccpos.impostosIntegration.domain.ProprietarioIntegration;
import tccpos.impostosIntegration.exceptions.InvalidDataException;
import tccpos.impostosIntegration.integrations.ImpostosProxy;

@Service
@Slf4j
public class ProprietariosServiceImp implements ProprietariosService {

	@Autowired
	private ImpostosProxy impostosProxy;

	public Iterable<Proprietario> find(final String documento) throws InvalidDataException {
		List<Proprietario> proprietarios = new ArrayList<>();
		
		List<ProprietarioIntegration> proprietarioTemp = impostosProxy.findProprietariosByNumeroDocumento(documento);
		
		proprietarioTemp.parallelStream().forEach(d -> {
			proprietarios.add(convertProprietarioIntegrationToProprietario(d));

		});

		log.info("{}", proprietarios);

		return proprietarios;
	}

	private Proprietario convertProprietarioIntegrationToProprietario(ProprietarioIntegration proprietarioIntegration) {
		Proprietario proprietario = new Proprietario();
		List <Imovel> imoveis = new ArrayList<>();
		proprietario.setId(proprietarioIntegration.getId());
		proprietario.setNumeroContribuinte(proprietarioIntegration.getNumeroContribuinte());
		proprietario.setDocumento(proprietarioIntegration.getDocumento());
		proprietario.setTipoDocumento(proprietarioIntegration.getTipoDocumento());
		proprietario.setTipoContribuinte(proprietarioIntegration.getTipoContribuinte());
		proprietario.setNome(proprietarioIntegration.getNome());
		proprietario.setCadastramento(proprietarioIntegration.getCadastramento());
		proprietarioIntegration.getImoveis().parallelStream().forEach(e ->{
			ImovelIntegration imovelTemp = new ImovelIntegration();
			try {
				imovelTemp = impostosProxy.findImovelByID(e);
			} catch (InvalidDataException e1) {
				log.error("{}", e1);
			}
			List <Imposto> impostos = new ArrayList<>();
			imovelTemp.getImpostos().parallelStream().forEach(f ->{
				try {
					impostos.add(impostosProxy.findImpostoByID(f));
				} catch (InvalidDataException e1) {
					log.error("{}", e1);
				}
			});
			imoveis.add(new Imovel(imovelTemp.getId(),imovelTemp.getCodigoCadastro()
			,imovelTemp.getLogradouro(),imovelTemp.getNumero(),imovelTemp.getComplemento()
			,imovelTemp.getBairro(),imovelTemp.getCep(),imovelTemp.getUso(),imovelTemp.getAreaTerreno()
			,imovelTemp.getAreaConstruida(),imovelTemp.getAreaFrente(),imovelTemp.getValorAreaConstruida()
			,impostos));
		});
		proprietario.setImoveis(imoveis);
		
		return proprietario;
	}

}
