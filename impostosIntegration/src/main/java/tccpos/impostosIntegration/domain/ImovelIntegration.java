package tccpos.impostosIntegration.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor	
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImovelIntegration implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	Integer id;
	@JsonProperty("codigo_cadastro")
	String codigoCadastro;
	String logradouro;
	String numero;
	String complemento;	
	String bairro;	
	String cep;
	String uso;
	@JsonProperty("area_terreno")
	String areaTerreno;
	@JsonProperty("area_construida")
	String areaConstruida;
	@JsonProperty("area_frente")
	String areaFrente;
	@JsonProperty("valor_construida")
	String valorAreaConstruida;
	List <Long> impostos;
}
