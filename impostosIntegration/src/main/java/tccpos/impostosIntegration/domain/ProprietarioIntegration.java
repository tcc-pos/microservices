package tccpos.impostosIntegration.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor	
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProprietarioIntegration implements Serializable{

	private static final long serialVersionUID = 1L;

	String id;
	@JsonProperty("numero_contribuinte")
	String numeroContribuinte;
	String documento;
	@JsonProperty("tipo_documento")
	String tipoDocumento;
	@JsonProperty("tipo_contribuinte")
	String tipoContribuinte;
	String nome;
	@JsonProperty("data_cadastramento")
	LocalDate cadastramento;
	@JsonProperty("imoveis")
	List <Long> imoveis;

}
