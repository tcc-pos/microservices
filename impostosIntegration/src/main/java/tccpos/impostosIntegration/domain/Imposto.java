package tccpos.impostosIntegration.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Imposto implements Serializable{

	private static final long serialVersionUID = 1L;

	Integer id;
	@JsonProperty("numero_documento")
	String numeroDocumento;
	@JsonProperty("data_base")
	LocalDate dataBase; // ou ano exercicio
	@JsonProperty("vencimento_parcelas")
	List <LocalDate> dataVencimentoParcelas;
	
	String total; //valor total documento
//http://legislacao.prefeitura.sp.gov.br/leis/lei-10235-de-16-de-dezembro-de-1986
}
