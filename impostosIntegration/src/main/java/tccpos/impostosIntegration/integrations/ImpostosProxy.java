package tccpos.impostosIntegration.integrations;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import tccpos.impostosIntegration.domain.Imovel;
import tccpos.impostosIntegration.domain.ImovelIntegration;
import tccpos.impostosIntegration.domain.Imposto;
import tccpos.impostosIntegration.domain.Proprietario;
import tccpos.impostosIntegration.domain.ProprietarioIntegration;
import tccpos.impostosIntegration.exceptions.InvalidDataException;


@FeignClient(name = "impostosProxy", url = "${impostos-url}")
public interface ImpostosProxy {

	@GetMapping(value = "/impostos/{id}")
	public Imposto findImpostoByID(@PathVariable(value = "id") final Long id)
			throws InvalidDataException;

	@GetMapping(value = "/proprietarios/{id}")
	public Proprietario findProprietarioByID(@PathVariable(value = "id") final Long id)
			throws InvalidDataException;
	
	@GetMapping(value = "/proprietarios")
	public List <ProprietarioIntegration> findProprietariosByNumeroDocumento(@RequestParam(value = "documento") final String documento)
			throws InvalidDataException;
			
	@GetMapping(value = "/imoveis/{id}")
	public ImovelIntegration findImovelByID(@PathVariable(value = "id") final Long id)
			throws InvalidDataException;
	
}
