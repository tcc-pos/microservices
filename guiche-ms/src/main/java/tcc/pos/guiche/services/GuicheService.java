package tcc.pos.guiche.services;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import tcc.pos.guiche.domain.Guiche;
import tcc.pos.guiche.exceptions.NotFoundException;
import tcc.pos.guiche.repository.GuicheRepository;

@Service
public class GuicheService {

	@Autowired
	private GuicheRepository guicheRepository;

	public Iterable<Guiche> find(final String nome) {
		if (StringUtils.isEmpty(nome)) {
			return guicheRepository.findAll();
		} else {
			return guicheRepository.findByNomeIgnoreCase(nome);
		} 
	}

	public Guiche findById(final Long id) throws NotFoundException {
		return guicheRepository.findById(id).orElseThrow(() -> new NotFoundException());
	}

	public Guiche create(final Guiche guiche) {
		return guicheRepository.save(guiche);
	}

	public Guiche update(final Long id, final Guiche guiche) throws NotFoundException {
		Guiche regiaoEntity = guicheRepository.findById(id).orElseThrow(() -> new NotFoundException());
		BeanUtils.copyProperties(guiche, regiaoEntity);

		return guicheRepository.save(regiaoEntity);
	}

	public void delete(final Long id) throws NotFoundException {
		Guiche regiaoEntity = guicheRepository.findById(id).orElseThrow(() -> new NotFoundException());
		guicheRepository.delete(regiaoEntity);
	}

}
