package tcc.pos.guiche.exceptions;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class CfgAdivice {

	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_GATEWAY)
	@ExceptionHandler({ RuntimeException.class })
	public String[] handleRunTimeException(RuntimeException ex) {
		log.error(ex.getMessage());
		return new String[] { "Internal error" };
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_GATEWAY)
	@ExceptionHandler({ Exception.class })
	public String[] handleRunTimeException(Exception ex) {
		log.error(ex.getMessage());
		return new String[] { "Internal error" };
	}

	@ResponseBody
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public String[] violacaoDeConstraint(final ConstraintViolationException ex) {
		return new String[] { ex.getMessage() };
	}

	@ResponseBody
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public String[] argumentosInvalidos(final MethodArgumentNotValidException ex) {
		return ex.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage()).toArray(String[]::new);
	}

	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public String[] validationError(NotFoundException ex) {
		return ex.getErrors();
	}

}