package tcc.pos.guiche.repository;

import org.springframework.data.repository.CrudRepository;

import tcc.pos.guiche.domain.Guiche;

public interface GuicheRepository extends CrudRepository<Guiche, Long> {

	public Iterable<Guiche> findByNomeIgnoreCase(final String nome);
	
}
