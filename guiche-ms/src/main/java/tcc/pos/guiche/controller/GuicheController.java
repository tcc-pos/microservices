package tcc.pos.guiche.controller;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import tcc.pos.guiche.domain.Guiche;
import tcc.pos.guiche.exceptions.NotFoundException;
import tcc.pos.guiche.services.GuicheService;

@RestController
@RequestMapping
@Tag(name = "guiches")
public class GuicheController {

	@Autowired
	private GuicheService guicheService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Iterable<Guiche> find(@QueryParam(value = "nome") final String nome) {
		return guicheService.find(nome);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Guiche findById(@PathVariable final Long id) throws NotFoundException {
		return guicheService.findById(id);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.CREATED)
	public Guiche create(@RequestBody final Guiche guiche) {
		return guicheService.create(guiche);
	}

	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Guiche update(@PathVariable("id") final Long id, @RequestBody final Guiche guiche) throws NotFoundException {
		return guicheService.update(id, guiche);
	}

	@DeleteMapping(value = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") final Long id) throws NotFoundException {
		guicheService.delete(id);
	}

}
