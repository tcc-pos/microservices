package tccpos.consultasMedicasIntegration.domain;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)

public class ExameProcedimento implements Serializable {

	private static final long serialVersionUID = -3746127067968098632L;
	private Integer id;
	private String grupo;//procedimento cirurgico ou exames
	private String subGrupo;//cirurgia de catarata ou exame de sangue
	private String nome;
	private LocalDate data;

}
