package tccpos.consultasMedicasIntegration.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)

public class UnidadeSaude implements Serializable {

	private static final long serialVersionUID = -6655911211919582050L;

	private Integer id;
	private String cnes; //Cadastro Nacional de Estabelecimentos de Saúde ref: https://wiki.saude.gov.br/cnes/index.php/P%C3%A1gina_principal
	private String cnpj; //Cadastro Nacional de pessoa juridica será sempre da SECRETARIA MUNICIPAL DE SAUDE DO MUNICIPIO
	private String tipoEstabelecimento; //PORTARIA Nº 2.022, DE 7 DE AGOSTO DE 2017(*) ref: http://bvsms.saude.gov.br/bvs/saudelegis/gm/2017/prt2022_15_08_2017_rep.html
	private String nomeFantasia;
	private String endereco;
	private String cep;
	private String telefone;
	private String email;

}
