package tccpos.consultasMedicasIntegration.domain;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)

public class Imunizacao implements Serializable {

	private static final long serialVersionUID = 4250915202847427438L;

	private Integer id;
	private LocalDate dataAdministracao;
	private String dose;//dose única, primeira dose, segunda dose, terceira dose, quarta dose, primeiro reforço ou segundo reforço
	private String nomeImunobiologico;

}
