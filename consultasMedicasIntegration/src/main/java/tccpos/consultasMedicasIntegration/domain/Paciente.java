package tccpos.consultasMedicasIntegration.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)

public class Paciente implements Serializable {

	private static final long serialVersionUID = 2042699068398980551L;
	private Long id;//prontuario
	private String cpf;
	private String cns; //Cartão Nacional de Saúde 
	private String nome;
	private String nomeMae;
	private String sexo;
	private String raca;//(branca, preta, parda, amarela, indígena e "sem informação")
	private String nacionalidade;
	private String municipioNascimento;
	private String ufNascimento;
	private String email;
	private String telefone;
	private String endereco;
	private String cep;
	private String identidade;
	private String representanteLegal;
	private String historicoAlergico;
	private String diagnosticosAtivos;
	private LocalDate nascimento;
	private List<Atendimento> consultas;
	private List <Imunizacao> imunizacoes;
	private List <ExameProcedimento> exames;

}
