package tccpos.consultasMedicasIntegration.domain;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)

public class Agendamento implements Serializable {

	private static final long serialVersionUID = 4250915602847427438L;
	
	private Integer idAgendamento;
	private LocalDate dataAgendamento;
	private Integer quantidadeAtendimentos;
	private String especialidade;
	private UnidadeSaude unidadeSaude;
	private Medico medico;

}
