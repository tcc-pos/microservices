package tccpos.consultasMedicasIntegration.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Atendimento implements Serializable {

	private static final long serialVersionUID = 4250915602847427448L;

	private Integer id;
	private LocalDate data;
	private String tipo; //consulta, pronto atendimento e internação, por exemplo
	private String especialidade;
	private String status;//aberto,encerrado ou cancelado
	private LocalDateTime inicio;
	private LocalDateTime fim;
	private String motivoConsulta;
	private String historiaDoencaAtual;
	private String historicoFamiliar;
	private String exameFisico;
	private String hipoteseDiagnostico;
	private Boolean diagnosticoConfirmado;
	private String terapia;
	private String codigoAtestado;
	private String codigoEncaminhamento;
	private UnidadeSaude unidadeSaude;
	private Medico medico;

}
