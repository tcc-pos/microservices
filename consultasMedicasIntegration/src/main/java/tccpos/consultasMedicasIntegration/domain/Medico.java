package tccpos.consultasMedicasIntegration.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)

public class Medico implements Serializable {

	private static final long serialVersionUID = -3746127067968098631L;
	private Integer id;
	private String crm;
	private String cpf;
	private String nome;
	private LocalDate nascimento;

}
