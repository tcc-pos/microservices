package tccpos.consultasMedicasIntegration.controller;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import tccpos.consultasMedicasIntegration.domain.Paciente;
import tccpos.consultasMedicasIntegration.exceptions.InvalidDataException;
import tccpos.consultasMedicasIntegration.services.PacientesService;

@RestController
@RequestMapping({ "/pacientes" })
@Tag(name = "pacientes")
public class PacientesController {

	@Autowired
	private PacientesService pacientesServices;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponse(responseCode = "200", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Paciente.class))))
	public Iterable<Paciente> findByCpf(@QueryParam(value = "cpf") final String cpf) throws InvalidDataException {
		return pacientesServices.find(cpf);
	}

}
