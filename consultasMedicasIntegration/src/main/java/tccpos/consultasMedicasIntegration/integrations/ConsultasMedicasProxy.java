package tccpos.consultasMedicasIntegration.integrations;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import tccpos.consultasMedicasIntegration.domain.Atendimento;
import tccpos.consultasMedicasIntegration.domain.AtendimentoIntegration;
import tccpos.consultasMedicasIntegration.domain.ExameProcedimento;
import tccpos.consultasMedicasIntegration.domain.Imunizacao;
import tccpos.consultasMedicasIntegration.domain.Medico;
import tccpos.consultasMedicasIntegration.domain.Paciente;
import tccpos.consultasMedicasIntegration.domain.UnidadeSaude;
import tccpos.consultasMedicasIntegration.exceptions.InvalidDataException;


import java.util.List;

@FeignClient(name = "consultasMedicasProxy", url = "${consultas-url}")
public interface ConsultasMedicasProxy {

	@GetMapping(value = "/atendimentos")
	public List<AtendimentoIntegration> findAtendimentosByPaciente(@RequestParam("paciente") final Long id) throws InvalidDataException;

	@GetMapping(value = "/unidade_saude/{id}")
	public UnidadeSaude findUnidadeSaudeByID(@PathVariable(value = "id") final Long id)
			throws InvalidDataException;

	@GetMapping(value = "/unidade_saude")
	public List<UnidadeSaude> findUnidadesSaude(@RequestParam(value = "cnpj") final String cpnj)
			throws InvalidDataException;

	@GetMapping(value = "/medicos/{id}")
	public Medico findMedicoByID(@PathVariable(value = "id") final Long id)
			throws InvalidDataException;
	
	@GetMapping(value = "/pacientes/{id}")
	public Paciente findPacienteByID(@PathVariable(value = "id") final Long id)
			throws InvalidDataException;
	
	@GetMapping(value = "/pacientes")
	public List<Paciente> findPacientesByCpf(@RequestParam(value = "cpf") final String cpf)
			throws InvalidDataException;
	
	@GetMapping(value = "/imunizacoes")
	public List<Imunizacao> findImunizacoesByPaciente(@RequestParam(value = "paciente") final Long paciente)
			throws InvalidDataException;

	@GetMapping(value = "/exames-procedimentos/")
	public List<ExameProcedimento> findExameProcedimentoByPaciente(@RequestParam(value = "paciente") final Long paciente) throws InvalidDataException;

}
