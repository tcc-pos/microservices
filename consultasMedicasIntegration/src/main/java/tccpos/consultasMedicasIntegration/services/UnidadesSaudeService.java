package tccpos.consultasMedicasIntegration.services;

import tccpos.consultasMedicasIntegration.domain.UnidadeSaude;
import tccpos.consultasMedicasIntegration.exceptions.InvalidDataException;

public interface UnidadesSaudeService {

	public Iterable<UnidadeSaude> find(final String cnpj) throws InvalidDataException;

}
