package tccpos.consultasMedicasIntegration.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import tccpos.consultasMedicasIntegration.domain.Atendimento;
import tccpos.consultasMedicasIntegration.domain.AtendimentoIntegration;
import tccpos.consultasMedicasIntegration.domain.ExameProcedimento;
import tccpos.consultasMedicasIntegration.domain.Imunizacao;
import tccpos.consultasMedicasIntegration.domain.Medico;
import tccpos.consultasMedicasIntegration.domain.Paciente;
import tccpos.consultasMedicasIntegration.domain.UnidadeSaude;
import tccpos.consultasMedicasIntegration.exceptions.InvalidDataException;
import tccpos.consultasMedicasIntegration.integrations.ConsultasMedicasProxy;

@Service
@Slf4j
public class PacientesServiceImp implements PacientesService {

	@Autowired
	private ConsultasMedicasProxy consultasMedicasProxy;

	public Iterable<Paciente> find(final String cpf) throws InvalidDataException {
		List<Paciente> pacientes = consultasMedicasProxy.findPacientesByCpf(cpf);
		log.info("{}", pacientes);
		
		pacientes.parallelStream().forEach(c -> {
			try {
				List <Atendimento> consultas = new ArrayList<>();
				List<AtendimentoIntegration> consultasIntegration = consultasMedicasProxy.findAtendimentosByPaciente(c.getId());
				log.info("{}", consultasIntegration);
				consultasIntegration.parallelStream().forEach(d -> {
					consultas.add(convertAtendimentoIntegrationToAtendimento(d));
				});
				List <Imunizacao> imunizacoes = consultasMedicasProxy.findImunizacoesByPaciente(c.getId());
				log.info("{}", imunizacoes);
				List <ExameProcedimento> exames = consultasMedicasProxy.findExameProcedimentoByPaciente(c.getId());
				log.info("{}", exames);
				c.setConsultas(consultas);
				c.setImunizacoes(imunizacoes);
				c.setExames(exames);
			} catch (InvalidDataException e) {
				log.error("{}", e);
			}

		});
		

		return pacientes;
	}

	private Atendimento convertAtendimentoIntegrationToAtendimento(AtendimentoIntegration atendimentoIntegration) {
		Atendimento atendimento = new Atendimento();
		try {
			UnidadeSaude unidadeSaude = consultasMedicasProxy.findUnidadeSaudeByID(atendimentoIntegration.getUnidadeSaude());
			Medico medico = consultasMedicasProxy.findMedicoByID(atendimentoIntegration.getMedico());
			atendimento.setId(atendimentoIntegration.getId());
			atendimento.setData(atendimentoIntegration.getData());
			atendimento.setTipo(atendimentoIntegration.getTipo());
			atendimento.setEspecialidade(atendimentoIntegration.getEspecialidade());
			atendimento.setStatus(atendimentoIntegration.getStatus());
			atendimento.setInicio(atendimentoIntegration.getInicio());
			atendimento.setFim(atendimentoIntegration.getFim());
			atendimento.setMotivoConsulta(atendimentoIntegration.getMotivoConsulta());
			atendimento.setHistoriaDoencaAtual(atendimentoIntegration.getHistoriaDoencaAtual());
			atendimento.setHistoricoFamiliar(atendimentoIntegration.getHistoricoFamiliar());
			atendimento.setExameFisico(atendimentoIntegration.getExameFisico());
			atendimento.setHipoteseDiagnostico(atendimentoIntegration.getHipoteseDiagnostico());
			atendimento.setDiagnosticoConfirmado(atendimentoIntegration.getDiagnosticoConfirmado());
			atendimento.setTerapia(atendimentoIntegration.getTerapia());
			atendimento.setCodigoAtestado(atendimentoIntegration.getCodigoAtestado());
			atendimento.setCodigoEncaminhamento(atendimentoIntegration.getCodigoEncaminhamento());
			atendimento.setUnidadeSaude(unidadeSaude);
			atendimento.setMedico(medico);
		} catch (InvalidDataException e) {
			log.error("{}", e);
		}
	return atendimento;
	}
}




