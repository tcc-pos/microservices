package tccpos.consultasMedicasIntegration.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import tccpos.consultasMedicasIntegration.domain.UnidadeSaude;
import tccpos.consultasMedicasIntegration.exceptions.InvalidDataException;
import tccpos.consultasMedicasIntegration.integrations.ConsultasMedicasProxy;

@Service
@Slf4j
public class UnidadesSaudeServiceImp implements UnidadesSaudeService {

	@Autowired
	private ConsultasMedicasProxy consultasMedicasProxy;

	public Iterable<UnidadeSaude> find(final String cnpj) throws InvalidDataException {
		List<UnidadeSaude> unidadesSaude = consultasMedicasProxy.findUnidadesSaude(cnpj);
		log.info("{}", unidadesSaude);
		
		return unidadesSaude;
	}

	
}




