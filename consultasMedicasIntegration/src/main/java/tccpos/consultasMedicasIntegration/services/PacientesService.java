package tccpos.consultasMedicasIntegration.services;

import tccpos.consultasMedicasIntegration.domain.Paciente;
import tccpos.consultasMedicasIntegration.exceptions.InvalidDataException;

public interface PacientesService {

	public Iterable<Paciente> find(final String cpf) throws InvalidDataException;

}
