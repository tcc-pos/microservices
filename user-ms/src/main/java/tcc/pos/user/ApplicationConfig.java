package tcc.pos.user;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@Configuration
public class ApplicationConfig {

	@Bean
	public OpenAPI customOpenAPI(@Value("${spring.application.description}") String appDesciption,
			@Value("${spring.application.version}") String appVersion,
			@Value("${spring.application.name}") String appName) {
		return new OpenAPI().info(new Info().title(appName).version(appVersion).description(appDesciption));
	}

}