package tcc.pos.user.services;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import tcc.pos.user.domain.Role;
import tcc.pos.user.exceptions.NotFoundException;
import tcc.pos.user.repository.RoleRepository;

@Service
public class RoleServiceImp implements RoleService {

	@Autowired
	private RoleRepository roleRepository;

	public Iterable<Role> find(final String nome) {
		if (StringUtils.isEmpty(nome)) {
			return roleRepository.findAll();
		} else {
			return roleRepository.findByNomeIgnoreCase(nome);
		}
	}

	public Role findById(final Long id) throws NotFoundException {
		return roleRepository.findById(id).orElseThrow(() -> new NotFoundException());
	}

	public Role create(final Role Role) {
		return roleRepository.save(Role);
	}

	public Role update(final Long id, final Role Role) throws NotFoundException {
		Role roleEntity = roleRepository.findById(id).orElseThrow(() -> new NotFoundException());
		BeanUtils.copyProperties(Role, roleEntity);

		return roleRepository.save(roleEntity);
	}

	public void delete(final Long id) throws NotFoundException {
		Role regiaoEntity = roleRepository.findById(id).orElseThrow(() -> new NotFoundException());
		roleRepository.delete(regiaoEntity);
	}

}
