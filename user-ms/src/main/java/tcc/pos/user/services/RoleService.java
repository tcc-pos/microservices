package tcc.pos.user.services;

import tcc.pos.user.domain.Role;
import tcc.pos.user.exceptions.NotFoundException;

public interface RoleService {
	
	public Iterable<Role> find(final String nome);
	public Role findById(final Long id) throws NotFoundException;
	public Role create(final Role Role);
	public Role update(final Long id, final Role Role) throws NotFoundException;
	public void delete(final Long id) throws NotFoundException;

}
