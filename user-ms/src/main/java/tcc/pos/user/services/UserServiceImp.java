package tcc.pos.user.services;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import tcc.pos.user.domain.Role;
import tcc.pos.user.domain.RoleEnum;
import tcc.pos.user.domain.User;
import tcc.pos.user.exceptions.AccessDeniedException;
import tcc.pos.user.exceptions.InvalidDataException;
import tcc.pos.user.exceptions.NotFoundException;
import tcc.pos.user.repository.RoleRepository;
import tcc.pos.user.repository.UserRepository;
import tcc.pos.user.utils.UserValidation;

@Service
public class UserServiceImp implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	public Iterable<User> find() {
		return userRepository.findAll();
	}

	public User findById(final Long id) throws NotFoundException, AccessDeniedException {
		UserValidation.validateUserAccess(id);
		return userRepository.findById(id).orElseThrow(() -> new NotFoundException());
	}

	@SuppressWarnings("unlikely-arg-type")
	public User create(final User user) throws InvalidDataException {
		if (userRepository.findByUsername(user.getUsername()).isPresent()) {
			throw new InvalidDataException(new String[] { "User already exists" });
		}

		if (user.getRoles() == null || user.getRoles().size() == 0) {
			List<Role> roles = StreamSupport
					.stream(roleRepository.findByNomeIgnoreCase(RoleEnum.USER.name()).spliterator(), false)
					.collect(Collectors.toList());
			user.setRoles(new HashSet<>(roles));
		} else {
			for (Role role : user.getRoles()) {
				roleRepository.findById(role.getId())
						.orElseThrow(() -> new InvalidDataException(new String[] { "Not found role" }));
			}
		}

		if (user.getRoles().contains(RoleEnum.ADMIN.name()) && !UserValidation.isAdmin()) {
			throw new InvalidDataException(new String[] { "you are not admin" });
		}

		if (user.getEnabled() == null) {
			user.setEnabled(true);
		}

		user.setPassword(treatPassword(user.getPassword()));

		User newUser = userRepository.save(user);
		newUser.setPassword(null);

		return newUser;
	}

	public User update(final Long id, final User user)
			throws NotFoundException, InvalidDataException, AccessDeniedException {

		UserValidation.validateUserAccess(id);

		User userEntity = userRepository.findById(id).orElseThrow(() -> new NotFoundException());
		Boolean tmpEnabled = userEntity.getEnabled();

		if (!user.getUsername().equals(userEntity.getUsername())) {
			throw new InvalidDataException(new String[] { "Cant change username" });
		}

		BeanUtils.copyProperties(user, userEntity);

		if (userEntity.getEnabled() == null) {
			userEntity.setEnabled(tmpEnabled);
		}

		for (Role role : userEntity.getRoles()) {
			roleRepository.findById(role.getId())
					.orElseThrow(() -> new InvalidDataException(new String[] { "Not found role" }));
		}

		userEntity.setPassword(treatPassword(userEntity.getPassword()));

		return userRepository.save(userEntity);
	}

	public void delete(final Long id) throws NotFoundException {
		User regiaoEntity = userRepository.findById(id).orElseThrow(() -> new NotFoundException());
		userRepository.delete(regiaoEntity);
	}

	private String treatPassword(final String password) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.encode(password);
	}

}
