package tcc.pos.user.services;

import tcc.pos.user.domain.User;
import tcc.pos.user.exceptions.AccessDeniedException;
import tcc.pos.user.exceptions.InvalidDataException;
import tcc.pos.user.exceptions.NotFoundException;

public interface UserService {

	public Iterable<User> find();

	public User findById(final Long id) throws NotFoundException, AccessDeniedException;

	public User create(final User user) throws InvalidDataException;

	public User update(final Long id, final User User)
			throws NotFoundException, InvalidDataException, AccessDeniedException;

	public void delete(final Long id) throws NotFoundException;

}
