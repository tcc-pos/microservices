package tcc.pos.user.repository;

import org.springframework.data.repository.CrudRepository;

import tcc.pos.user.domain.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {

	public Iterable<Role> findByNomeIgnoreCase(final String nome);
	
}
