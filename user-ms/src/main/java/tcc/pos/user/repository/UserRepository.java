package tcc.pos.user.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import tcc.pos.user.domain.User;

public interface UserRepository extends CrudRepository<User, Long> {

	public Optional<User> findByUsername(final String username);
	
}
