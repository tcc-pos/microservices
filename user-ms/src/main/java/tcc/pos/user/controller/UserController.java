package tcc.pos.user.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import tcc.pos.user.domain.User;
import tcc.pos.user.exceptions.AccessDeniedException;
import tcc.pos.user.exceptions.InvalidDataException;
import tcc.pos.user.exceptions.NotFoundException;
import tcc.pos.user.services.UserService;

@RestController
@RequestMapping("users")
@Tag(name = "users")
@Validated
public class UserController {

	@Autowired
	private UserService userService;

	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Iterable<User> find() {
		return userService.find();
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public User findById(@PathVariable final Long id) throws NotFoundException, AccessDeniedException {
		return userService.findById(id);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.CREATED)
	public User create(@Valid @RequestBody final User user) throws InvalidDataException {
		return userService.create(user);
	}

	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public User update(@PathVariable("id") final Long id, @Valid @RequestBody final User user)
			throws NotFoundException, InvalidDataException, AccessDeniedException {
		return userService.update(id, user);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(value = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") final Long id) throws NotFoundException {
		userService.delete(id);
	}

}
