package tcc.pos.user.controller;

import javax.validation.Valid;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import tcc.pos.user.domain.Role;
import tcc.pos.user.exceptions.NotFoundException;
import tcc.pos.user.services.RoleService;

@RestController
@RequestMapping("roles")
@Tag(name = "roles")
@Validated
public class RoleController {

	@Autowired
	private RoleService roleService;

	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Iterable<Role> find(@QueryParam(value = "nome") final String nome) {
		return roleService.find(nome);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Role findById(@PathVariable final Long id) throws NotFoundException {
		return roleService.findById(id);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.CREATED)
	public Role create(@Valid @RequestBody final Role Role) {
		return roleService.create(Role);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Role update(@PathVariable("id") final Long id, @Valid @RequestBody final Role Role)
			throws NotFoundException {
		return roleService.update(id, Role);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(value = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") final Long id) throws NotFoundException {
		roleService.delete(id);
	}

}
