package tcc.pos.user.exceptions;

import lombok.Getter;

public class AccessDeniedException extends Exception {

	private static final long serialVersionUID = -4496861348764733959L;

	@Getter
	private final String[] errors;

	public AccessDeniedException(final String[] erros) {
		super();
		this.errors = erros;
	}

	public AccessDeniedException() {
		super();
		this.errors = new String[] { "Access Denied" };
	}

}
