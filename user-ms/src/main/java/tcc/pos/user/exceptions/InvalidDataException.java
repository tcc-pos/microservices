package tcc.pos.user.exceptions;

import lombok.Getter;

public class InvalidDataException extends Exception {

	private static final long serialVersionUID = 3857698137429235523L;

	@Getter
	private final String[] errors;

	public InvalidDataException(final String[] erros) {
		super();
		this.errors = erros;
	}

	public InvalidDataException() {
		super();
		this.errors = new String[] { "Invalid data" };
	}

}
