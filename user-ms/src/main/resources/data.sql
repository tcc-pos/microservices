-- tabelas
insert into User (id, username, password, enabled) values(hibernate_sequence.NEXTVAL, '123', '$2y$12$MKYTZM4XVcLt0IqVYJJ.2.Jll9U1Xk17ti2rjoED58zWbFMC.ByuO', true);
insert into User (id, username, password, enabled) values(hibernate_sequence.NEXTVAL, '321', '$2y$12$6mlYpxjJUm988YrkmYA8b.G05uWNuh9.86l3nspHg0d8C/Z5LPfpW', true);
insert into User (id, username, password, enabled) values(hibernate_sequence.NEXTVAL, '11111111111', '$2y$12$MKYTZM4XVcLt0IqVYJJ.2.Jll9U1Xk17ti2rjoED58zWbFMC.ByuO', true);
insert into User (id, username, password, enabled) values(hibernate_sequence.NEXTVAL, '22222222222', '$2y$12$6mlYpxjJUm988YrkmYA8b.G05uWNuh9.86l3nspHg0d8C/Z5LPfpW', true);

insert into Role (id, nome) values (hibernate_sequence.NEXTVAL, 'USER');
insert into Role (id, nome) values (hibernate_sequence.NEXTVAL, 'ADMIN');

insert into user_roles (user_id, roles_id) values (select id from User where username = '123', select id from Role where nome = 'USER');
insert into user_roles (user_id, roles_id) values (select id from User where username = '321', select id from Role where nome = 'ADMIN');
insert into user_roles (user_id, roles_id) values (select id from User where username = '11111111111', select id from Role where nome = 'USER');
insert into user_roles (user_id, roles_id) values (select id from User where username = '22222222222', select id from Role where nome = 'ADMIN');
